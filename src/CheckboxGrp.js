import React, { Component } from 'react';
import Checkbox from './Checkbox';
import PropTypes from 'prop-types';



class CheckboxGrp extends Component {

  
  componentWillMount = () => {
    this.selectedCheckboxes = new Set();
  
  }

  clearChkBoxes(){
       this.selectedCheckboxes.clear();
    
  }
  
  
  toggleCheckbox = (label, chk_box_id) => {

    if (this.selectedCheckboxes.has(chk_box_id)) {
      this.selectedCheckboxes.delete(chk_box_id);
    } else {
      this.selectedCheckboxes.add(chk_box_id);
    }
    this.props.handleChkgroupChange(this.props.grpName,this.selectedCheckboxes);
  }

  createCheckbox = chkbx => (
    <Checkbox
      label={chkbx[1]}
      handleCheckboxChange={this.toggleCheckbox}
      key={chkbx[0]}
      chk_box_id={chkbx[0]}
      
    />
  )

  render() {
    return (
        <div>
            {
                this.props.data.map(this.createCheckbox)
            }
        </div>
    );
  }
}

CheckboxGrp.propTypes = {
  handleChkgroupChange: PropTypes.func.isRequired,
};
export default CheckboxGrp;