import React, { Component } from 'react';
import {ButtonToolbar, DropdownButton, MenuItem} from 'react-bootstrap';
import axios from 'axios';
import {Typeahead, tokenContainer} from 'react-bootstrap-typeahead';
import { DefaultButton, IconButton, IButtonProps } from 'office-ui-fabric-react/lib/Button';

const MyCustomToken = tokenContainer(props => (
   <div className="rbt-token-removeable" title={props.option.value}>
      {props.option.image != null && props.option.image.length > 0 ? (
      <img className="ManufacturesButtonImage" src={"Логотипы/" + props.option.image} alt="" />   
      ) : (<p className="rbt-token-removeable-text"> {props.option.value} </p>
      )
   }
   <div className="rbt-token-close-button" onClick={props.onRemove}>x</div>
  </div>
));


function ChkBoxValue(item, index) {
   var chkbx = {id: item[0], value: item[1], image: item[2]};
   return chkbx;
}

const  filterClick = (ev,e) => {
	 alert("a");
 }
 
class PricesGridHeader extends Component {
 constructor(props) {
    super(props);
    this.state = {
    data: [],
    sortText: "Выберите параметр",
    sortId: 0
    };
  }

  
  
  componentWillMount() {
    if (this.props.maincat) {
    axios.get(process.env.REACT_APP_AGROTRADE_API + '/manufacturers/' + this.props.maincat)
    .then(res => {
            this.setState({data: res.data.dataset.map(ChkBoxValue)});
            
      });
    }
  }

  reloadData(uroute) {
    
    axios.get(process.env.REACT_APP_AGROTRADE_API + '/manufacturers/' + uroute)
    .then(res => {
            this.setState({data: res.data.dataset.map(ChkBoxValue)});
      });
 }

 handleSelect = (evtKey, evt) => {
   this.setState({sortText: evt.target.text,
                 sortId: evtKey});
   this.props.setsort(evtKey);
 }
 

 
  render(){
   
    return (
    <div className="ManufacturesAndSorting">    
 
       <div className="ms-Grid">
		  <div className="ms-Grid-row">
			<div className="ms-Grid-col ms-sm6 ms-md8 ms-lg10"><Typeahead
				 labelKey="value"
				 multiple
				 options={this.state.data}
				 onChange = {this.props.manufacturerHandler}
				 placeholder="Поиск по производителю"
				 renderToken={(selectedItem, onRemove) => (
					<MyCustomToken key={selectedItem.id} onRemove={onRemove} option={selectedItem} test="Hello"/>
					)}
				 /> </div>
			<div className="ms-Grid-col ms-sm6 ms-md4 ms-lg2">
			
			<ButtonToolbar>
					<DropdownButton title={this.state.sortText} id="dropdown-size-medium" onSelect={this.handleSelect}>
						<MenuItem eventKey="1">Цена (от большего)</MenuItem>
						<MenuItem eventKey="2">Цена (от меньшего)</MenuItem>
						
					</DropdownButton>
				</ButtonToolbar>
			
			</div>
		  </div>
		</div>
		

    </div>
    
 
    );
  }
}

export default PricesGridHeader;

/*<ButtonManufacturesGrp
                data={this.state.data}
                handleChkgroupChange={this.props.chkBoxHandler}
                >
                </ButtonManufacturesGrp>
*/