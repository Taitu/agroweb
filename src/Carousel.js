import React, { Component } from 'react';
const Carousel = () => {
    return (
<div className="carousel slide carousel-fade" data-ride="carousel" id="asCarousel">


	    <div className="carousel-inner" role="listbox">
	        <div className="item active"/>

	        <div className="item"/>

	        <div className="item"/>

	    </div>
	</div>
    );
};

export default Carousel;