import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import * as Api from './Api';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Panel, PanelType } from 'office-ui-fabric-react/lib/Panel';
import { TextField } from 'office-ui-fabric-react/lib/TextField';


class DealerModal extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            data: {},
            showPanel: false
        }
        
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.showDealerInfo = this.showDealerInfo.bind(this);
    }

 getInitialState() {
    return { showPanel: false };
  }

  close() {
    this.setState({ showPanel: false });
  }

  open(dealerId) {
    Api.get_dealerinfo(this.showDealerInfo, dealerId);
    this.setState({ showPanel: true });
  }
  
  showDealerInfo(dataset){
    this.setState({ data: dataset });
  }

   _setShowPanel(showPanel: boolean): () => void {
    return (): void => {
      this.setState({ showPanel });
    };
  }

/* 
 render() {
    var data = this.state.data;
    return (
      <Modal show={this.state.showPanel} onHide={this.close} bsSize="large" aria-labelledby="contained-modal-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">Информация о дистрибьютере</Modal.Title>
        </Modal.Header>
        <Modal.Body>
                    <h4> Информация о продавце: </h4>
                    <ul className= 'dealer-popup-list'>
                        <li>Имя: <p>{data["name"]}</p></li>
                        <li>ИНН: <p>{data["inn"]}</p></li>
                        <li>Email: <p>{data["email"]}</p></li>
                        <li>Веб сайт: <a href={data["web_site"]}>{data["web_site"]}</a></li>
                        <li>Город: <p>{data["city"]}</p></li>
                        <li>Телефон: <p>{data["phone"]}</p></li>
                        <li>Регион: <p>{data["region"]}</p></li>
                        <li>Адрес: <p>{data["address"]}</p></li>
                    </ul>

        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.close}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
  */
render() {
    var data = this.state.data;
	var _company_logo = <p><i className="ms-Icon ms-Icon--ContactCard" aria-hidden="true"></i> {data["name"]}</p>;
	//var _med = _company_logo + data["name"];
	console.log(data);
    return (
		<Panel
          isOpen={ this.state.showPanel }
          onDismiss={ this._setShowPanel(false) }
          type={ PanelType.medium }
          headerText= { _company_logo }
        >
		<div className="ms-Grid">
		  <p className="ms-font-xl">Сведения о Продавце</p>
 
		  <div className="ms-Grid-row margin-bot-10">
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg4">Регион:</div>
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg8">{data["region"]}</div>
		  </div>
		  
		  <div className="ms-Grid-row margin-bot-10">
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg4">Город:</div>
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg8">{data["city"]}</div>
		  </div>
		  
		  <div className="ms-Grid-row margin-bot-10">
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg4">Адрес:</div>
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg8">{data["address"]}</div>
		  </div>
		  
		
		  
			
			<p className="ms-font-xl  margin-top-30">Сведения о Товарах и сделках</p>
			
		  <div className="ms-Grid-row margin-bot-10">
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg4">Товаров в системе:</div>
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg8"><i className="ms-Icon ms-Icon--CloudUpload" aria-hidden="true"></i></div>
		  </div>	
			
		  <div className="ms-Grid-row margin-bot-10">
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg4">Всего успешных сделок:</div>
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg8">Доступно для зарегистрированных</div>
		  </div>
		  
		  <div className="ms-Grid-row margin-bot-10">
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg4">Рейтинг:</div>
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg8"><i className="ms-Icon ms-Icon--CloudUpload" aria-hidden="true"></i></div>
		  </div>
		  
		  
			<p className="ms-font-xl margin-top-30">Связаться с Продавцом</p>
			
		  <div className="ms-Grid-row margin-bot-10">
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg4">Ваше имя:</div>
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg8"><TextField
          disabled={ false }
          placeholder='ФИО'
        /></div>
		  </div>	
		  
		  <div className="ms-Grid-row margin-bot-10">
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg4">Телефон:</div>
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg8"><TextField
          disabled={ false }
          placeholder=''
        /></div>
		  </div>
			
		  <div className="ms-Grid-row margin-bot-10">
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg4">Сообщение:</div>
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg8"><TextField
          disabled={ false }
          multiline
          rows={ 4 }
        /></div>
		  </div>
		  
		  <div className="ms-Grid-row margin-bot-10">
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg4"> </div>
			<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg8"><Button onClick={this.close}>Отправить данные</Button></div>
		  </div>
		
		</div>
		
	
		
        </Panel>
		
	
     
    );
  }
 
}

export default DealerModal;
