/**
 * Created by NikitaB on 12/11/17.
 */
import { createStore, applyMiddleware } from 'redux'
import { logger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';

import reducers from './RootReducer';

let middlewares = [thunkMiddleware];
process.env.NODE_ENV !== 'production' && middlewares.push(logger);

export const store = createStore(
  reducers,
  applyMiddleware(...middlewares)
);
