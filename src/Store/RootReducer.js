/**
 * Created by NikitaB on 12/11/17.
 */
import { combineReducers } from "redux";

import { UserReducer as user} from 'UserMenu/reducers';

const reducers = combineReducers({
  user
});

export default reducers;
