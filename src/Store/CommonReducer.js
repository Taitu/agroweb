/**
 * Created by NikitaB on 12/11/17.
 */
export default function CommonReducer (initState = {}, actionNames = []) {
  return function (state = initState, action) {
    if (actionNames.indexOf(action.type) !== -1) {
      let data = {...action};
      delete data.type;
      return {...state, ...data};
    }
    return state;
  }
}
