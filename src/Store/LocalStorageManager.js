/**
 * Created by NikitaB on 12/11/17.
 */
class LocalStorageManagerClass {

  set(key, value) {
    try {
      value = JSON.stringify(value)
    } catch (e) {}
    localStorage.setItem(key, value);
  }

  get(key) {
    let value = localStorage.getItem(key);
    try {
      value = JSON.parse(value)
    } catch (e) {}
    return value;
  }
}

const LocalStorageManager = new LocalStorageManagerClass();
export default LocalStorageManager;