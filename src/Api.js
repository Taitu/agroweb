import axios from 'axios';


export function call_catfilters(callback, jsonFilters) {
      axios.post( process.env.REACT_APP_AGROTRADE_API+ 'catfilters', jsonFilters)
          .then(res => {
                  callback(res.data.dataset);
            });
}

export function call_categories(callback, catId) {
    axios.get( process.env.REACT_APP_AGROTRADE_API+ '/categories/'+ catId)
        .then(res => {
                callback(catId, res.data.dataset, 0);
          });
}


export function	call_maincats(callback) {
        axios.get(process.env.REACT_APP_AGROTRADE_API+ '/maincategories')
        .then(res => {
                callback(res.data.dataset);
          });
		}

export function	send_order(callback, data) {
        axios.post(process.env.REACT_APP_AGROTRADE_API+ '/createorder', data)
        .then(res => {
                callback(res.data.result);
          });
		}

export function get_dealerinfo(callback, dealerId) {
    axios.get( process.env.REACT_APP_AGROTRADE_API+ '/dealer/'+ dealerId)
        .then(res => {
                callback(res.data.dealer);
          });
}
