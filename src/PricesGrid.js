import InfiniteScroll from 'react-infinite-scroller';
import React, { Component } from 'react';
import { Table, Button} from 'react-bootstrap';
import {
  CompoundButton,
  IButtonProps
} from 'office-ui-fabric-react/lib/Button';

import {
  Rating,
  RatingSize
} from 'office-ui-fabric-react/lib/Rating';

import {
  Breadcrumb, IBreadcrumbItem
} from 'office-ui-fabric-react/lib/Breadcrumb';


import { TextField } from 'office-ui-fabric-react/lib/TextField';
import {
  DetailsList,
  DetailsListLayoutMode,
  Selection,
  IColumn
} from 'office-ui-fabric-react/lib/DetailsList';
import { MarqueeSelection } from 'office-ui-fabric-react/lib/MarqueeSelection';
import { Label } from 'office-ui-fabric-react/lib/Label';
import axios from 'axios';
import PricesModal from './PricesModal';
import DealerModal from './DealerModal';
//import { initializeIcons } from 'office-ui-fabric-react/lib/Icons';

import { Link } from 'office-ui-fabric-react/lib/Link';
import { Image, ImageFit } from 'office-ui-fabric-react/lib/Image';
import { createListItems, isGroupable } from '@uifabric/example-app-base';

import { autobind } from 'office-ui-fabric-react/lib/Utilities';

import Disqus from 'disqus-react';

import {
  Spinner,
  SpinnerSize
} from 'office-ui-fabric-react/lib/Spinner';

//initializeIcons(/* optional base url */); 
var disqus_developer = 1;
let _items: any[] = [];
let _product = null;

if (_items.length === 0) {
      for (let i = 0; i < 10; i++) {
        _items.push({
          key: i,
          name: 'Item ' + i,
          value: i
        });
      }
    }

// Фото, Название, категория, Производитель, Параметры, Цена от, За объем, Предложения
let _columns_marketgrid: IColumn[] = [
  {
    key: 'm_image',
    name: 'Фото',
	headerClassName: 'no-display',
    fieldName: 'm_image',
    minWidth: 50,
    maxWidth: 100,
    isResizable: true,
    ariaLabel: '-'
  },
   {
    key: 'm_name',
    name: 'Название',
	headerClassName: 'no-display',
    fieldName: 'm_name',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: '-'
  }
  ,
   {
    key: 'm_cat',
    name: 'Категория',
	headerClassName: 'no-display',
    fieldName: 'm_cat',
    minWidth: 50,
    maxWidth: 100,
    isResizable: true,
    ariaLabel: '-'
  }
  ,
   {
    key: 'm_manuf',
    name: 'Производитель',
	headerClassName: 'no-display',
    fieldName: 'm_manuf',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: '-'
  }
  
  // Параметры, Цена от, За объем, Предложения
  
  ,
   {
    key: 'm_params',
    name: 'Параметры',
	headerClassName: 'no-display',
    fieldName: 'm_params',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: '-'
  }
  ,
   {
    key: 'm_price',
    name: 'Цена от',
	headerClassName: 'no-display',
    fieldName: 'm_price',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: '-'
  }
  ,
   {
    key: 'm_amount',
    name: 'Ед.изм',
	headerClassName: 'no-display',
    fieldName: 'm_amount',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: '-'
  }
  ,
   {
    key: 'm_offers',
    name: 'Предложения',
	headerClassName: 'no-display',
    fieldName: 'm_offers',
    minWidth: 150,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: '-'
  }
  
  ]
	
let _columns_params: IColumn[] = [
  {
    key: 'param_name',
    name: 'Параметр',
	headerClassName: 'no-display',
    fieldName: 'param_name',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: 'Параметр продукта'
  },
   {
    key: 'param_value',
    name: 'Значение',
	headerClassName: 'no-display',
    fieldName: 'param_value',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: 'Значение параметра'
  }]	

let _columns: IColumn[] = [
  {
    key: 'column1',
    name: 'Продавец',
    fieldName: 'name',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: 'Operations for name'
  },
  {
    key: 'column2',
    name: 'Предложение',
    fieldName: 'value',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: 'Operations for value'
  },
   {
    key: 'column3',
    name: 'Условия',
    fieldName: 'options',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: 'Operations for value'
  },
   {
    key: 'column4',
    name: 'Стоимость',
    fieldName: 'price',
	isSorted: true,
    minWidth: 80,
    maxWidth: 100,
    isResizable: true,
    ariaLabel: 'Operations for value'
  }
  ,
   {
    key: 'column4',
    name: 'Валюта',
    fieldName: 'curr',
    minWidth: 80,
    maxWidth: 100,
    isResizable: true,
    ariaLabel: 'Operations for value'
  },
   {
    key: 'column5',
    name: 'Заказ',
    fieldName: 'btn',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: 'Operations for value'
  },
];


class PricesGrid extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            prices: [],
			pricesdetails: [],
			priceslist: [],
            products: [],
            add_products: [],
            modalItems: [],
            hasMoreItems: true,
            nextHref: null,
            datatype: "grouped",
            reportUrl: '/products',
            showModal: false,
            nextPage: 0,
            isLoading: false,
            singleClicked: false,
            productId: undefined
        };
        this.product = null;
		this.state.pricesdetails = _items;
    }

shouldComponentUpdate(nextProps, nextState) {
  if (this.state != nextState) {
    return true;  
  }
  else {
    return false;
  }
  
}

mapParams = p => ({
	'id': p[0],
	'key': p[0],
	'param_name': p[1],
	'param_value': p[4]
})
    
mapPrices = p => ({
	'key': p[6],
	'id': p[0],
	'name': <a onClick = {() => this.refs.DealerModal.open(p[7])}>{p[5]}</a>,
	'value': p[2],
	'options': p[4],
	'price': p[0],
	'curr': p[3],
	'btn': <a className="btnAddOrder" onClick={() => {
								
								this.refs.modalBasket.handleOpenModal(this.get_data_pricemodal(p))
							  }}><i className="ms-Icon ms-Icon--AddTo" aria-hidden="true"></i> В список заказа</a>
	
})
    
createLines(data, index) {
    if (this.state.datatype === "grouped" ) {
        this.createGroupedPrices(data, index);
    }
  }
  
  get_data_pricemodal = (p) => {
	  var price = this.get_prices_dict(p);
	  price['product'] = this.get_product_dict(_product);
	  return price;
  }
  
  
  get_product_dict = (data) => {
    var product = {
        'id' : data[0],
        'name': data[1],
        'manufacturer' : data[2],
        'price_min': data[3],
        'price_max': data[4],
        'offer_count': data[5],
        'main_pars': data[6],
        'logo': data[7],
        'unit': data[8],
        'cname1': data[9],
        'cname2': data[10],
        'cname3': data[11],
        'cid1': data[12],
        'cid2': data[13],
        'cid3': data[14],
        'add_pars': data[15]
    };
    
    return product;    
  }

  get_prices_dict = (data) => {
    var price = {
        'price': data[0],
        'actual_date': data[1],
        'price_options': data[2],
        'currency': data[3],
        'price_unit': data[4],
        'dealer': data[5],
        'price_id': data[6],
        'dealer_id': data[7],
    };
    return price;
    
  }
  

  createGroupedPrices = (data, index) => {
    if (data === undefined) { return 0};
    var product = this.get_product_dict(data)
    var pars = product['main_pars']

	var _price_from = "";
	var _price_to = "";
	var _exchange_rate = 63;
	var _price_convert = 0;
    

	_price_from = product['price_min'];
	var _isrc = "";
	if (product['offer_count'] === 1) _isrc = " вариант ";
	if ((product['offer_count'] > 1) && (product['offer_count'] < 5)) _isrc = " варианта ";
	if (product['offer_count'] >= 5 ) _isrc = " вариантов ";	
  
	// Dios: those values can be loaded from REST
	// var spacingA = 10;
	//<i className="fa fa-tencent-weibo as-big-icon" aria-hidden="true"></i>
    
	//var __category_image = "/Логотипы/" + product['logo'];
	var __category_image = "/i/" + product['logo'];
	if (product['logo'] == null) __category_image = "http://static.agrisale.ru/tools/image_cat.php?id=" + product['id'] + "&cat=" + product['cname3'] + "&rand=" + Math.random();
	//var __category_img_size = 250;


	// Временно расставляем EPD иконки, надо сделать чтобы брались сами
	var _technology_icon = (!index%5)?<img src='i/epd.png' alt="" />:"";

	// Всего просмотров
	// var _total_views = index + "00";
	
	// Дополнительный текст по продукту
	// var _product_badge = (!index%3)?" Лучшие продажи! ":"";
	
	var _previewImage = {
          name: 'Revenue stream proposal fiscal year 2016 version02.pptx',
          url: 'http://bing.com',
		  onClick:  (e) => this.loadSingle(product['id'])  ,
         // previewImageSrc: "/default.jpg",
          //iconSrc: TestImages.iconPpt,
          width: 144
        };

	var price = {
          key: product['id'],
          m_name: product['name'],
		  m_image: <img src={__category_image} className='as-product-image' />,
          m_price: _price_from,
		  m_manuf: product['manufacturer'],
		  m_cat: product['cname3'],
		  m_params: pars.map( function(param, index) {
                        return(
                            <dl key={index} className='asSingleParams'>
                                <dt className='ms-font-s-plus ms-fontWeight-regular'>{param[1]}:</dt>
                                <dd className='ms-font-s-plus ms-fontWeight-light'>{param[4]}</dd>
                            </dl>
                            )
                        }),
		  m_amount: product['unit'],
		  m_offers: <CompoundButton
						 primary={ true }
						 description={product['offer_count'] + _isrc}
						 value={product['id']}
						 iconProps={ { iconName: 'Add' } }
                         onClick={ (e) => this.loadSingle(product['id']) }
						 >
						 Показатьaaaaa
						 </CompoundButton>
							 
		  
        };

		   //this.loadSingle(e).bind(this)
	if (index === 3)
	{
		//add advert
		this.state.items.push(
			<tr key={"A"+ product['offer_count']} className='as-tr-split'>
				<td colSpan="3" className="as-product-banner"> banner </td>
			</tr>
		)
	}
  return price;
  }
    
  reDrawProducts = () => {
    this.setState({ datatype:"grouped", prices:[], items:[]})
  }

  findProductData = () => {
    return this.state.products.find((item) => {if (item[0] === parseInt(this.props.selectedFilters["product_id"])) 
                                                        return true; 
                                                        else return false;
                                                        })
    
  }
  
  createSingleHeader = (data) => {
    if (data === undefined) { return 0};
    var product = this.get_product_dict(data);
    var __category_image = "/i/" + product['logo'];
	if (product['logo'] == null) __category_image = "/i/default_prod.jpg";

    // console.log(data)
/*
	this.state.items.push(
		 <tr key="asSingleItemHeaderA" >
			<td colSpan="4">
					<span className="asSingleBread"><button onClick={this.reDrawProducts.bind(this)}>Назад</button> <i className="fa fa-angle-right" aria-hidden="true"></i>
                    </span>
			</td>
		 </tr>
	); 		
*/
	this.state.items.push(
		 <tr key="asSingleItemHeaderA" >
			<td colSpan="4">
					<Breadcrumb
					  items={ [
						{ text: product['cname1'], 'key': '12', href: '/search/category/'+product['cid1'], onClick: this.reDrawProducts.bind(this) },
						{ text: product['cname2'], 'key': '123'
            , href: '/search/category/'+product['cid1']+'/subcat/' + product['cid2']
            , onClick: this.reDrawProducts.bind(this) },
						{ text: product['cname3'], 'key': '1234'
            , href: '/search/category/'+product['cid1']+'/subcat/' + product['cid3']
            , onClick: this.reDrawProducts.bind(this), isCurrentItem: true }
						
					  ] }
					  maxDisplayedItems={ 3 }
					  
					  ariaLabel={ 'Категории товара' }
					/> 
					
			</td>
		 </tr>
	); 		

/*
	this.state.items.push(
	<tr key="asSingleItemHeaderB" className='as-tr-split'>
           
		 	<td colSpan="2" className="as-middle-all">
					<p style={{marginTop: "10px"}}>
					<dl className='asSingleParams'>
						<dt className='asSingleParamsKey'><img  src={__category_image}  alt="" /></dt>
						<dd className='asSingleParamsValue'></dd>
					</dl>
					</p>
			</td>
			<td colSpan="2">
					<p className='as-product-header'>{product['name']}</p>
					<p></p>
					<p className='asSingleOptionsA'><i className="fa fa-arrow-circle-right" aria-hidden="true"></i> Сведения о товаре</p>
                      {product['main_pars'].map( function(param, index) {
                        return(
                            <dl key={index} className='asSingleParams'>
                                <dt className='asSingleParamsKey'><span className='asSingleInherit'>{param[1]}:</span></dt>
                                <dd className='asSingleParamsValue'>{param[4]}</dd>
                            </dl>
                            )
                        })}

					
					
					
					<p className='asSingleOptionsB'><i className="fa fa-arrow-circle-right" aria-hidden="true"></i> Дополнительные параметры</p>
                      {product['add_pars'].map( function(param, index) {
                        return(
                            <dl key={index} className='asSingleParams'>
                                <dt className='asSingleParamsKey'><span className='asSingleInherit'>{param[1]}:</span></dt>
                                <dd className='asSingleParamsValue'>{param[4]}</dd>
                            </dl>
                            )
                        })}
			</td>
		 </tr>
	);
		//<img src={__category_image}  className="image-300" alt="" />
	*/
	
	
	
	var t__category_image = "http://static.agrisale.ru/tools/image_cat.php?id=" + product['id'] + "&cat=" + product['cname3'] + "&rand=" + Math.random();
	this.state.items.push(
		<tr key="asSingleItemHeaderB" className='as-tr-split'>
			<td colSpan="3">
			<img src={t__category_image} className="image-50-fll"/>
		
					<p className='ms-fontColor-themePrimary ms-fontWeight-semibold as-border-bottom ms-font-xxl as-xxl'>{product['name']}</p>
					  
					<p></p>
					<p className='ms-fontSize-xl ms-fontWeight-semibol ms-fontColor-themePrimary'>Сведения о товаре</p>
                      
					 <DetailsList
							items={product['main_pars'].map(this.mapParams)}
							columns={ _columns_params }
							setKey='set'
							layoutMode={ DetailsListLayoutMode.fixedColumns }
							checkboxVisibility= 'hidden'
							
						  />
						  
			</td></tr>);
			
	if (data) {
			this.createPrices2(data);
            //this.state.prices.map(this.createPrices.bind(this, productData));
			//this.state.pricesdetails.map();
			//this.setState({pricesdetails: []});
			//console.log(this.state.prices);
			//this.createPrices2.bind(this, productData);
			//console.log(this.state.pricesdetails);
        }
		this.createFPrices();
        this.state.items.push();
	
	this.state.items.push(
		<tr key="asSingleItemHeaderC" className='as-tr-split'>
			<td colSpan="3">
					<p className='ms-fontSize-xl   ms-fontWeight-semibol ms-fontColor-themePrimary'>Дополнительные параметры</p>
					<DetailsList
							items={product['add_pars'].map(this.mapParams)}
							columns={ _columns_params }
							setKey='set'
							layoutMode={ DetailsListLayoutMode.fixedColumns }
							
							
						  />
                      
			</td>
			
			
		</tr>
	);
    /*
	this.state.items.push(
		 <tr key="asSingleItemHeaderC" >
			<td colSpan="4">
					<span className="asSingleHeadingA">Предлагаемые варианты и заказ продукта</span>
			</td>
		 </tr>
	);
	this.state.items.push(
		 <tr key="asSingleItemHeaderD" className="asSingleItemHeaderD">
			<td>Продавец</td>
			<td>Предложение</td>
			<td>Стоимость и условия</td>
			<td> </td>
			
		 </tr>
	);
	*/
  }

 createFPrices = () => {
	  
	  //let items = this.state.pricesdetails;
	  //console.log(_columns);
	  this.state.items.push(
	  <tr key={new Date()}><td colSpan="4">
					<div>
						<p className="ms-fontSize-xl  ms-fontWeight-semibol ms-fontColor-themePrimary">Текущие предложения</p>
						
						<MarqueeSelection selection={ this._selection }>
						  <DetailsList
							items={ this.state.pricesdetails }
							columns={ _columns }
							setKey='set'
							layoutMode={ DetailsListLayoutMode.fixedColumns }
							
							selectionPreservedOnEmptyClick={ true }
							ariaLabelForSelectionColumn='Toggle selection'
							ariaLabelForSelectAllCheckbox='Toggle selection for all items'
							onItemInvoked={ this._onItemInvoked }
						  />
						</MarqueeSelection>
					  </div>
		</td></tr>
					);
					this.state.items.push(
	  <tr><td colSpan="4">
	  Информация о ценах и продавцах взята из свободных источников и может отличаться от действительной. Пожалуйста, уточняйте цены у продавцов (поставщиков).
	  </td></tr>
					);
  }
  
  createPrices2 = (productData) => {
	  //price['product'] = this.get_product_dict(productData);
	  var _t = this.state.prices.map(this.mapPrices);
	  _product = productData;
	 // this.state.pricesdetails = []; 
	 //console.log(this.state.pricesdetails);
	  this.state.pricesdetails = _t;
	  //console.log("STATE PRICES");
	  //console.log(this.state.pricesdetails);
  }
/*   
  createPrices = (productData, data, index) => {
    var price = this.get_prices_dict(data);
    price['product'] = this.get_product_dict(productData);
    
    this.state.items.push(
            <tr key={index} className='as-tr-split'>
				<td className='as-middle-all'>
					<p className='asSingleDealer'><img src='i/_default_dealer_logo.png' className="asDealerLogo" alt="" /><p>
                    <a onClick = {() => this.refs.DealerModal.open(price['dealer_id'])}   className = 'dealer-button'>{price['dealer']}</a></p>
					<p>Рейтинг: 
						<i className="fa fa-star-o" aria-hidden="true"></i>
						<i className="fa fa-star-o" aria-hidden="true"></i>
						<i className="fa fa-star-o" aria-hidden="true"></i>
						<i className="fa fa-star-o" aria-hidden="true"></i>
						<i className="fa fa-star-o" aria-hidden="true"></i>
					</p>
					</p>
				</td>
		   
                <td className='asSingleTDMIDDLE'>
					<p className='asSingleOption'>{price['price_options']}</p>
                   
                   
                    
                </td>
               <td  className='asSingleTDMIDDLE'>
                    <p className='asSinglePriceA'><i className="fa fa-credit-card" aria-hidden="true"></i> {price['price']} {price['currency']} </p>
					<p className='asSinglePriceB'>({price['price_unit']})</p>
                    <p className='asSinglePriceC'>Актуально на {price['actual_date']}</p>
                    
                </td>
				<td  className='asSingleTDMIDDLE'>
				
					<Button onClick={() => this.refs.modalBasket.handleOpenModal(price)}>В список покупок</Button>
					
				</td>
              </tr>

            );
  }
*/
 createPrices = (productData, data, index) => {
    var price = this.get_prices_dict(data);
    price['product'] = this.get_product_dict(productData);
    
	//console.log(productData);
	
	 this.state.pricesdetails.push({
          key: index,
          name: price['dealer'],
          value: index
        });
	
 }

loadSingle = (t) =>  {
    this.setState({ datatype:"single",
                   reportUrl:  '/prices'});
    this.props.selectedFilters["product_id"] = t+"";
    delete this.props.selectedFilters["page"];
    delete this.props.selectedFilters["limit"];
    this.reloadItems(0);
}

loadSingleViaSearch = (t) =>  {
    this.setState({
        singleClicked: true,
        productId: t+""
    });
}


loadGroups = (selectedFilters) =>  {

    this.setState({ datatype:"grouped",
                   reportUrl: '/products',
                   prices: [],
                   products: [],
                   add_products: [],
				   priceslist: [],
                   items:[],
                   hasMoreItems: true});
    this.loadItems(0, selectedFilters);    
    
}

reloadItems (page) {

     var that = this;
      that.setState({prices: []
                    , items:[]
				   , priceslist: []
                    , hasMoreItems: true
                    });
        
}
  
  loadItems (page, selectedFilters) {

     var that = this;
     var filters = selectedFilters ? selectedFilters : this.props.selectedFilters;
     var jsonFilters = Object.assign({}, filters);
    //console.log(jsonFilters)
    axios.post(process.env.REACT_APP_AGROTRADE_API + this.state.reportUrl, jsonFilters)
      .then(function (res) {
        var moreItems = false;
        var page = Number(res.data.page);
        var limit = Number(res.data.limit);
        var total = Number(res.data.total);
        var empty =  null;
        
        
        if (((page+1)* limit) < total) {
            that.props.selectedFilters["page"] = page+1;
            that.props.selectedFilters["limit"] = limit;
            empty = new Array(1)
            moreItems = true;
        }
        if (that.state.reportUrl === '/products' ) {
            if ('prices_add' in res.data) {
              var price_items = res.data.prices.map(that.createGroupedPrices.bind(that));
              var price_items_add = res.data.prices_add.map(that.createGroupedPrices.bind(that));
              
                that.setState({products: that.state.products.concat(res.data.prices, res.data.prices_add),
                              hasMoreItems : moreItems,
                              priceslist: price_items.concat(price_items_add),
                              isLoading: false,
                              items: []});
            }
            else {
              var price_items = res.data.prices.map(that.createGroupedPrices.bind(that));
                if (moreItems) {
                  that.setState({products: that.state.products.concat(res.data.prices),
                                priceslist: that.state.priceslist.concat(price_items),
                                hasMoreItems : moreItems, items: [],
                                isLoading: false,
                                nextPage: page+1});
                }
                else {
                  that.setState({products: that.state.products.concat(res.data.prices),
                                priceslist: that.state.priceslist.concat(price_items),
                                hasMoreItems : moreItems, items: [],
                                isLoading: false,
                                nextPage: page+1});
                 
                }
            }
        }
        else if (that.state.reportUrl === '/prices' ) {
            that.setState({prices: res.data.prices, hasMoreItems : moreItems, items: []});
        }

        if (that.state.singleClicked) {
            that.loadSingle(that.state.productId);
            that.setState({
                singleClicked: false,
                productId: undefined
            });
        }
      });
       
      
  
}

_renderItemColumn = (item: any, index: number, column: IColumn) => {
  let fieldContent = item[column.fieldName];

  switch (column.key) {
    case 'thumbnail':
      return <Image src={ fieldContent } width={ 50 } height={ 50 } imageFit={ ImageFit.cover } />;

    case 'name':
      return <Link href='#'>{ fieldContent }</Link>;

    case 'color':
      return <span data-selection-disabled={ true } style={ { color: fieldContent } }>{ fieldContent }</span>;

	case "m_price":
      return <span>{ fieldContent } руб.</span>;
	  
    default:
      return <span>{ fieldContent }</span>;
  }
}

_renderMissingItem = (index) => {
  console.log(index)
  if (!this.state.isLoading) {
    this.state.isLoading = true;
    this.loadItems(this.state.nextPage);
  }
}

_renderDidMount = (item,index) => {
	console.log("didMount" + index);
}
_ldidUpdate = (list) => {
	console.log(list);
  console.log('didupdate');
	//list.props.onColumnResize();
	list.focusIndex();
	//list.forceUpdate();
}

render() {
    
    const loader = <div key='loader-key' className="loader"><Spinner size={ SpinnerSize.large } label='Ищем для Вас лучшие предложения...' ariaLive='assertive' /></div>;
    var ldata = null;
    
    if (this.state.datatype === "grouped"){
    //const pitems = this.state.priceslist;
		ldata = (
				<InfiniteScroll
				pageStart={0}
				loadMore={this.loadItems.bind(this)}
				hasMore={this.state.hasMoreItems}
				loader={loader}>

				<DealerModal ref='DealerModal'/>
				<PricesModal ref="modalBasket"/>
				
				<DetailsList
					items={ this.state.priceslist }
					setKey='set'  
					columns={ _columns_marketgrid }
					layoutMode= { DetailsListLayoutMode.justified }
          //onRenderMissingItem={ this._renderMissingItem.bind(this) }
					//onDidUpdate= {this._ldidUpdate.bind(this) }
					//onRenderRow={ this._renderRow }
					//onRowDidMount={ this._renderDidMount }
					//onRenderItemColumn={ this._renderItemColumn }
					//onColumnHeaderClick={ this._onColumnClick }
					//onItemInvoked={ this._onItemInvoked }
					//onColumnHeaderContextMenu={ this._onColumnHeaderContextMenu }
				  />
				</InfiniteScroll>
				
			);
			this.state.items.push(<p></p>);
		 //Иначе подвисает рендер
        
    }
    if (this.state.datatype === "single"){
        var productData = this.findProductData();
    	this.createSingleHeader(productData);
		
        /* was here */
		
		var disqusShortname = 'agrisales';
        let disqusConfig = {
            url: "http://agrisale.ru/search/product/" + this.props.selectedFilters["product_id"],
            identifier: "pr"+this.props.selectedFilters["product_id"],
            title: "PR-" + this.props.selectedFilters["product_id"],
        };
		//console.log(disqusConfig);
		
		this.state.items.push(<tr key={`2 ${new Date()}`}>
            <td>
                <div className="article">
                    <h1>{this.props.selectedFilters["product_id"]}</h1>
                    <Disqus.CommentCount shortname={disqusShortname} config={disqusConfig}>
                        Комментарии и отзывы о продукте
                    </Disqus.CommentCount>
                    <p>Внутренний артикул товара: {this.props.selectedFilters["product_id"]}</p>
                    <Disqus.DiscussionEmbed shortname={disqusShortname} config={disqusConfig} />
                </div>
            </td>
        </tr>);
			
		ldata = (
			<InfiniteScroll
				pageStart={0}
				loadMore={this.loadItems.bind(this)}
				hasMore={this.state.hasMoreItems}
				loader={loader}>
			<DealerModal ref='DealerModal'/>
			<PricesModal ref="modalBasket"/>
			
			<Table responsive>
				<tbody>
				
				
				{this.state.items}
				
				</tbody>
			</Table>
			</InfiniteScroll>
			
		);
    }
	if (this.state.datatype === "preinfo")
	{
		
	}
    return ldata;
    
    /*
    else {
        var productData = this.findProductData();
    	this.createSingleHeader(productData);
        if (productData) {
            this.state.prices.map(this.createPrices.bind(this, productData));
        }
       
    }
    */
	/*
    return (
        <InfiniteScroll
            pageStart={0}
            loadMore={this.loadItems.bind(this)}
            hasMore={this.state.hasMoreItems}
            loader={loader}
            
        >
        <DealerModal ref='DealerModal'/>
        <Table responsive>
            <tbody>
            <PricesModal ref="modalBasket" 
            />
            
            {this.state.items}
            
            </tbody>
        </Table>
        </InfiniteScroll>
    );
    */
}

}
export default PricesGrid;

