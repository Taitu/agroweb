/**
 * Created by NikitaB on 12/11/17.
 */
import LocalStorageManager from 'Store/LocalStorageManager';
import axios from 'axios';

import { store } from 'Store/Store';


export const TOGGLE_LOGIN_FORM = 'TOGGLE_LOGIN_FORM';
export const SIGN_IN = 'SING_IN';
export const SET_USER_DATA = 'SET_USER_DATA';
export const SET_USER_DATA_ERROR = 'SET_USER_DATA_ERROR';
export const SET_USER_DATA_FIELD = 'SET_USER_DATA_FIELD';
export const SIGN_OUT = 'SIGN_OUT';

export const GET_ORDERS_LIST = 'GET_ORDERS_LIST';
export const SET_ORDERS_LIST = 'SET_ORDERS_LIST';
export const TOGGLE_ORDERS_LIST = 'TOGGLE_ORDERS_LIST';

const errorsLocales = {
  'User login must be unique': 'Пользователь с таким именем уже существует',
  'Unauthorized Access': 'Неправильный логин или пароль',
  'Email not provided': 'Пожалуйста, снимите ограничение на чтение адреса электронной почты в социальной сети.',
  'Internal Server Error': 'Ошибка. Обратитесь к администратору.',
  'User does not exists': 'Пользователя с таким именем не существует.'
};


export function getOrdersListAction () {
  store.dispatch(getOrdersList());
  return {
    type: GET_ORDERS_LIST,
    ordersLoading: true,
    showOrdersList: true
  }
}

export function setOrdersListAction (orders = false) {
  let res = {
    type: SET_ORDERS_LIST,
    ordersLoading: false
  };

  if (orders) {
    res.orders = orders;
  }
  return res;
}

export function toggleOrdersList (show=true) {
  return {
    type: TOGGLE_ORDERS_LIST,
    showOrdersList: show
  }
}

export function toggleLoginForm (show=true) {
  return {
    type: TOGGLE_LOGIN_FORM,
    showLoginForm: show,
    activation: false,
    error: null
  }
}

export function signIn (apiToken, activation=false) {
  LocalStorageManager.set('apiToken', apiToken);
  return {
    type: SIGN_IN,
    isLoggedIn: true,
    showLoginForm: activation,
    error: null,
    activation: activation,
    apiToken
  }
}

export function setUserDataField(key, value) {
  return {
    type: SET_USER_DATA_FIELD,
    key,
    value
  }
}

export function setUserDataError(error) {
  return {
    type: SET_USER_DATA_ERROR,
    error
  }
}

export function setUserData(userData) {
  return {
    type: SET_USER_DATA,
    userData
  }
}

export function signOut () {
  LocalStorageManager.set('apiToken', null);
  return {
    type: SIGN_OUT,
    apiToken: null,
    isLoggedIn: false,
    userData: {},
    error: null
  }
}


export function login(username, password) {
  return function (dispatch) {
    return axios.get(process.env.REACT_APP_AGROTRADE_API + '/token', {
      auth: {
        username: username,
        password: password
      }
    })
    .then(res => {
      if (res.data && res.data.token) {
        dispatch(signIn(res.data.token));
        dispatch(getUserData(res.data.token));
      }
    })
    .catch(res => {
      let msg = res.response.data.message || res.response.data;

      if (msg in errorsLocales) {
        msg = errorsLocales[msg];
      }
      dispatch(setUserDataError(msg));
    });
  }
}

export function getTokenWithSocial(socialName) {
  return function (dispatch) {
    dispatch(setUserDataError(null));
    return axios.get(process.env.REACT_APP_AGROTRADE_API + `/oauth/${socialName}/redirect`)
    .then(res => {
      if (res.data && res.data.url) {
        window.location.href = res.data.url;
      }
    })
    .catch(res => {
      let msg = res.response.data.message || res.response.data;

      if (msg in errorsLocales) {
        msg = errorsLocales[msg];
      }
      dispatch(setUserDataError(msg));
    });
  }
}

export function setTokenWithSocial(url) {
  return function (dispatch) {
    return axios.get(process.env.REACT_APP_AGROTRADE_API + url)
    .then(res => {
      if (res.data && res.data.token) {
        dispatch(signIn(res.data.token));
        dispatch(getUserData(res.data.token));
      }
    })
    .catch(res => {
      dispatch(toggleLoginForm(true));
      try {
        let msg = res.response.data.message || res.response.data;

        if (msg in errorsLocales) {
          msg = errorsLocales[msg];
        }
        dispatch(setUserDataError(msg));
      } catch (e) {
        console.error(res);
      }
    });
  }
}

export function registerUser(userData) {
  return function (dispatch) {
    return axios.put(process.env.REACT_APP_AGROTRADE_API + '/user', userData)
      .then(res => {
        if (res.data) {
          dispatch(login(userData.email, userData.password));
        }
      })
      .catch(res => {
        let msg = res.response.data.message || res.response.data;

        if (msg in errorsLocales) {
          msg = errorsLocales[msg];
        }
        dispatch(setUserDataError(msg));
      });
  }
}

export function getUserData(apiToken = LocalStorageManager.get('apiToken')) {
  return function (dispatch) {
    return axios.get(process.env.REACT_APP_AGROTRADE_API + '/user', {
      headers: {
        Authorization: `Bearer ${apiToken}`
      }
    })
    .then(res => {
      if (res.data) {
        dispatch(setUserData(res.data));
      }
    })
    .catch( res => {
      if (res.response && res.response.status == 401) {
        dispatch(signOut());
      }
    });
  }
}

export function updateUserData(userData, apiToken = LocalStorageManager.get('apiToken')) {
  return function (dispatch) {
    return axios.patch(process.env.REACT_APP_AGROTRADE_API + '/user', userData, {
      headers: {
        Authorization: `Bearer ${apiToken}`
      }
    })
    .then(res => {
      if (res.data) {
        dispatch(toggleLoginForm(false));
      }
    });
  }
}



export function getOrdersList(apiToken = LocalStorageManager.get('apiToken')) {
  return function (dispatch) {
    return axios.get(process.env.REACT_APP_AGROTRADE_API + '/user/orders', {
      headers: {
        Authorization: `Bearer ${apiToken}`
      }
    })
    .then(res => {
      if (res.data) {
        let ret_result = [];
        res.data.forEach( ord => {
          let data = JSON.parse(ord.products);
          delete ord.products;
          Object.keys(data).forEach( p_id => {
            ret_result.push({
              ...ord,
              ...data[p_id]
            })
          });
        });
        dispatch(setOrdersListAction(ret_result));
      }
    })
    .catch(res => {
      let msg = res.response.data.message || res.response.data;

      if (msg in errorsLocales) {
        msg = errorsLocales[msg];
      }
      dispatch(setOrdersListAction());
      alert(msg);
    });
  }
}

export function restoreForgetPassword(email) {
  return function (dispatch) {
    dispatch(setUserDataError(null));
    return axios.post(process.env.REACT_APP_AGROTRADE_API + `/user/reset_password`, {
      email: email
    })
    .then(res => {
      if (res.data && res.data.result === 'Ok') {
        dispatch(setUserDataError('На указанный вами E-mail выслано письмо с необходимыми данными для восстановления пароля.'));
      }
    })
    .catch(res => {
      let msg = res.response.data.message || res.response.data;

      if (msg in errorsLocales) {
        msg = errorsLocales[msg];
      }
      dispatch(setUserDataError(msg));
    });
  }
}
