/**
 * Created by NikitaB on 12/8/17.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DropdownButton, MenuItem } from 'react-bootstrap';

import LoginForm from './LoginForm';
import OrdersList from './OrdersList';

import { signOut, getUserData, toggleLoginForm, getOrdersListAction } from './actions';

class UserMenu extends Component {

  state = {
    login: false,
    registration: false,
    changePassword: false,
    editProfile: false
  };

  handleSelect(key) {
    switch(key) {
      case 'orders':
        this.props.dispatch(getOrdersListAction());
        break;
      case 'login':
        this.props.dispatch(toggleLoginForm(true));
        this.setState({login: true, registration: false, changePassword: false, editProfile: false});
        break;
      case 'register':
        this.props.dispatch(toggleLoginForm(true));
        this.setState({login: false, registration: true, changePassword: false, editProfile: false});
        break;
      case 'change_password':
        this.props.dispatch(toggleLoginForm(true));
        this.setState({login: false, registration: false, changePassword: true, editProfile: false});
        break;
      case 'profile':
        this.props.dispatch(toggleLoginForm(true));
        this.props.dispatch(getUserData());
        this.setState({login: false, registration: false, changePassword: false, editProfile: true});
        break;
      case 'logout':
        this.props.dispatch(toggleLoginForm(false));
        this.setState({login: false, registration: false, changePassword: false, editProfile: false});
        this.props.dispatch(signOut());
        break;
    }
  }

  closeLoginForm() {
    if (!this.props.user.activation) {
      this.props.dispatch(getUserData());

      this.props.dispatch(toggleLoginForm(false));
    }
  }

  componentWillMount() {
    if (this.props.user.isLoggedIn && this.props.user.apiToken) {
      this.props.dispatch(getUserData(this.props.user.apiToken));
    }
  }

  renderName() {
    return this.props.user.userData.name && this.props.user.userData.name.split(' ')[0];
  }

  render() {
    const isLogin = [
            <MenuItem key="0" header><b>{this.renderName()}</b></MenuItem>,
            <MenuItem key="1" eventKey="profile">Мой профиль</MenuItem>,
            <MenuItem key="2" eventKey="orders">Мои заказы</MenuItem>,
            <MenuItem key="3" eventKey="change_password">Сменить пароль</MenuItem>,
            <MenuItem key="4" eventKey="help">Помощь</MenuItem>,
            <MenuItem key="5" eventKey="logout">Выход</MenuItem>
          ],
          notLogin = [
            <MenuItem key="0" eventKey="login">Логин</MenuItem>,
            <MenuItem key="1" eventKey="register">Регистрация</MenuItem>
          ];

    return (
	<table>
        <tbody>
        <tr><td>
          <span>

            <DropdownButton style={{
                paddingTop: 0,
                borderWidth: 0,
                marginTop: 14
              }}
              title={

                <span>
                  <i className="ms-Icon ms-Icon--AzureKeyVault" aria-hidden="true"> </i> <a className="work-cabinet"> Рабочий кабинет</a>
                </span>
              } noCaret={true} id="dropdown-size-medium" onSelect={this.handleSelect.bind(this)}>
                {this.props.user.isLoggedIn && isLogin.map(i => i)}
                {!this.props.user.isLoggedIn && notLogin.map(i => i)}
            </DropdownButton>

            <LoginForm {...this.state}
                       closeHandler={this.closeLoginForm.bind(this)} />
            <OrdersList/>

          </span>
        </td><td>
        <span style={{
                paddingTop: 0,
                borderWidth: 0,
                marginTop: 14
              }}>
        <a className="work-cabinet" style={{
                display: 'block',

                paddingTop: 10
              }} href="http://static.agrisale.ru/help/"><i className="ms-Icon ms-Icon--Help" aria-hidden="true"></i>  Помощь и реклама</a>
        </span>
        </td></tr>
        </tbody>
    </table>
    )
  }
}

export default connect( state => ({
  user: state.user
}))(UserMenu);