/**
 * Created by NikitaB on 12/25/17.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Table } from 'react-bootstrap';

import { toggleOrdersList } from './actions';


class OrdersList extends Component {

  headers = [
    ['actual_date', 'Дата'],
    ['count', 'Количество'],
    ['dealer', 'Продавец'],
    ['price', 'Стоимость и условия'],
    ['price_options', 'Предложение']
  ];

  closeHandler() {
    this.props.dispatch(toggleOrdersList(false));
  }

  renderTable() {
    return (
      <Table responsive>
        <thead>
          <tr>
            {this.headers.map( h => <th key={h[0]}>{h[1]}</th>)}
          </tr>
        </thead>
        <tbody>
          {this.props.user.orders.map( (order, idx) =>
            <tr key={idx}>
              {this.headers.map( h => <td key={h[0]}>{order[h[0]]}</td>)}
            </tr>
          )}
        </tbody>
      </Table>
    )
  }

  render() {
    return (
      <Modal show={this.props.user.showOrdersList} onHide={this.closeHandler.bind(this)}>
        <Modal.Header closeButton>
          <Modal.Title>Ваши заказы</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {!this.props.user.orders.length ?
            <h4>Список заказов пуст.</h4>
            :
            this.renderTable()
          }

        </Modal.Body>
      </Modal>
    )
  }
}


export default connect( state => ({
  user: state.user
}))(OrdersList);
