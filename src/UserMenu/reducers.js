/**
 * Created by NikitaB on 12/11/17.
 */
import CommonReducer from 'Store/CommonReducer';
import LocalStorageManager from 'Store/LocalStorageManager';

import {
  SIGN_OUT, SIGN_IN, SET_USER_DATA, SET_USER_DATA_FIELD, TOGGLE_LOGIN_FORM, SET_USER_DATA_ERROR,
  GET_ORDERS_LIST, SET_ORDERS_LIST, TOGGLE_ORDERS_LIST
} from './actions';

const initState = {
  isLoggedIn: false,
  apiToken: null,
  showLoginForm: false,
  userData: {},
  error: null,
  ordersLoading: false,
  showOrdersList: false,
  orders: []
};

const apiToken = LocalStorageManager.get('apiToken');
if (apiToken) {
  initState.apiToken = apiToken;
  initState.isLoggedIn = true;
}

export const UserReducer = (state = initState, action) => {
    if (action.type === SET_USER_DATA_FIELD) {
      state.userData[action.key] = action.value;
      return {...state};
    }
    return CommonReducer(initState, [
      SIGN_OUT, SIGN_IN, SET_USER_DATA, SET_USER_DATA_ERROR, TOGGLE_LOGIN_FORM, GET_ORDERS_LIST,
      SET_ORDERS_LIST, TOGGLE_ORDERS_LIST
    ])(state, action);
};
