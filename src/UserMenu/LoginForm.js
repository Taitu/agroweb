/**
 * Created by NikitaB on 12/11/17.
 */
import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Modal, Button, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';

import {
  login, setUserDataField, registerUser, updateUserData, getTokenWithSocial, restoreForgetPassword
} from './actions';

import './LoginForm.css';

class LoginForm extends Component {

  registerHandler() {
    const data = this.props.user.userData;
    let errors = {};

    if (!data.password) {
      errors.password = "Введите пароль для входа в систему";
    }
    if (!data.password_confirm) {
      errors.password_confirm = "Введите пароль для подтверждения";
    }
    if (!data.email) {
      errors.email = "Введите электронную почту";
    }
    if (!!Object.keys(errors).length) {
      this.setErrors(errors);
      return;
    }
    this.clearErrors();
    this.props.dispatch(registerUser(data));
  }

  setErrors(errors) {
    let equipment = this.state.equipment;
    Object.keys(equipment).forEach( k => {
      if (k in errors) {
        equipment[k].help = errors[k];
        equipment[k].validationState = 'error';
      } else {
        delete equipment[k].help;
        delete equipment[k].validationState;
      }
    });
    this.setState({equipment: equipment});
  }

  clearErrors() {
    let equipment = this.state.equipment;
    Object.keys(equipment).forEach( k => {
      delete equipment[k].help;
      delete equipment[k].validationState;
    });
    this.setState({equipment: equipment});
  }

  changePasswordHandler() {
    this.props.dispatch(updateUserData(this.props.user.userData));
  }

  loginHandler() {
    this.props.dispatch(login(this.getFormElementValue('email'), this.getFormElementValue('password')));
  }

  changeProfileHandler() {
    this.props.dispatch(updateUserData(this.props.user.userData));
  }

  getFormElementValue(elem) {
    return this.refs.form.elements[elem].value;
  }

  /*
    'login': fields.String,
    'email': fields.String,
    'name': fields.String,
    'phone': fields.String,
    'entity': fields.String,
    'tin': fields.String(),
   */

  state = {
    socials: [
      {
        name: 'facebook',
        text: 'Facebook'
      },
      {
        name: 'ok',
        text: 'Однокласники'
      },
      {
        name: 'vk',
        text: 'ВКонтакте'
      }
    ],
    equipment: {
      email: {
        required: true,
        id: "email",
        type: "email",
        label: "Электронная почта",
        placeholder: "Адрес электронной почты для входа в систему"
      }
      ,password: {
        required: true,
        id: "password",
        label: "Пароль",
        type: "password"
      }
      ,password_confirm: {
        required: true,
        id: "password_confirm",
        label: "Повторите пароль",
        type: "password"
      }
      ,name: {
        required: true,
        id: "name",
        type: "text",
        label: "Имя",
        placeholder: "ФИО"
      }
      // ,fullname: <FieldGroup
      //   key="fullname"
      //   id="fullname"
      //   type="text"
      //   label="Фамилия"
      //   placeholder="Фамилия"
      // />
      // ,surname: <FieldGroup
      //   key="surname"
      //   id="surname"
      //   type="text"
      //   label="Отчество"
      //   placeholder="Отчество"
      // />
      ,phone: {
        required: true,
        key: "phone",
        id: "phone",
        type: "tel",
        label: "Телефон",
        placeholder: "+7xxxxxxxxxx"
      }
      ,entity: {
        key: "entity",
        id: "entity",
        type: "text",
        label: "Название организации",
        placeholder: "Название организации"
      }
      ,tin: {
        key: "tin",
        id: "tin",
        type: "text",
        label: "ИНН",
        placeholder: "ИНН организации"
      }
    }
  };

  renderByKeysOfEquipment() {
    let keyArray;

    if (this.props.login) {
      keyArray = ['email', 'password'];
    }
    else if (this.props.registration) {
      keyArray = false;
    }
    else if (this.props.changePassword || this.props.user.activation) {
      keyArray = ['password', 'password_confirm'];
    }
    else if (this.props.editProfile) {
      keyArray = Object.keys(this.state.equipment).filter( k => ['password', 'password_confirm'].indexOf(k) == -1);
    }

    return (keyArray || Object.keys(this.state.equipment)).map( key => this.renderFormControl(this.state.equipment[key]) );
  }

  renderFormControl({ id, label, help, validationState, ...props }) {
    props.value = this.props.user.userData[id] || '';
    return (
      <FormGroup key={id} controlId={id} validationState={validationState}>
        <ControlLabel>{label}</ControlLabel>
        <FormControl {...props} onChange={e => this.props.dispatch(setUserDataField(id, e.target.value))}/>
        {help && <HelpBlock>{help}</HelpBlock>}
      </FormGroup>
    )
  }

  renderTitle() {
    if (this.props.login) {
      return 'Вход в систему';
    }
    else if (this.props.registration) {
      return 'Регистрация пользователя';
    }
    else if (this.props.changePassword) {
      return 'Смена пароля';
    }
    else if (this.props.user.activation) {
      return 'Введите пароль';
    }
    else if (this.props.editProfile) {
      return 'Редактирование профиля';
    }
  }

  getConfirmFunction() {
    if (this.props.login) {
      return this.loginHandler.bind(this);
    }
    else if (this.props.registration) {
      return this.registerHandler.bind(this);
    }
    else if (this.props.changePassword || this.props.user.activation) {
      return this.changePasswordHandler.bind(this)
    }
    else if (this.props.editProfile) {
      return this.changeProfileHandler.bind(this);
    }
  }

  forgetPassword() {
    if (!this.props.user.userData.email) {
      this.setErrors({email: "Введите электронную почту"});
      return;
    }
    this.clearErrors();
    this.props.dispatch(restoreForgetPassword(this.props.user.userData.email))
  }

  render() {
    return (
      <Modal show={this.props.user.showLoginForm} onHide={this.props.closeHandler}>
        <Modal.Header closeButton={!this.props.user.activation}>
          <Modal.Title>{this.renderTitle()}</Modal.Title>
          <Modal.Title className="has-error"><span className="control-label">{this.props.user.error}</span></Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form ref="form">
            {this.renderByKeysOfEquipment()}
          </form>
          {this.props.login &&
          <a style={{paddingLeft: 3}}
             href="javascript: void(0)"
             onClick={this.forgetPassword.bind(this)}>
            Забыли пароль?
          </a>
          }
          {(this.props.registration || this.props.login) &&
          <div style={{paddingLeft: 3}}>
            <div>Зайти с помощью:</div>
            <div style={{display: 'table'}}>
              {this.state.socials.map((social, idx) => (
                <div style={{display: 'table-cell'}} key={idx}>
                  <div className={`loginBtn loginBtn--${social.name}`}
                       onClick={() => this.props.dispatch(getTokenWithSocial(social.name))}
                       title={social.text}>
                  </div>
                </div>
              ))}
            </div>
          </div>
          }
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.getConfirmFunction()}>Применить</Button>
          {!this.props.user.activation && <Button onClick={this.props.closeHandler}>Закрыть</Button>}
        </Modal.Footer>
      </Modal>
    )
  }
}

export default connect( state => ({
  user: state.user
}))(LoginForm);
