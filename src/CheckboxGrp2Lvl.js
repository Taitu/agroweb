import React, { Component } from 'react';
import {Accordion, Panel} from 'react-bootstrap';
import Checkbox from './Checkbox';
import PropTypes from 'prop-types';
import { Nav, INavProps } from 'office-ui-fabric-react/lib/Nav';


class CheckboxGrp2Lvl extends Component {

  
  componentWillMount = () => {
    this.selectedCheckboxes = new Set();
  
  }

    clearChkBoxes() {
       this.selectedCheckboxes.clear();
    
  }

  toggleCheckbox = (label, chk_box_id) => {

	alert(chk_box_id);
    if (this.selectedCheckboxes.has(chk_box_id)) {
      this.selectedCheckboxes.delete(chk_box_id);
    } else {
      this.selectedCheckboxes.add(chk_box_id);
    }
    
    this.props.handleChkgroupChange(this.props.grpName,this.selectedCheckboxes);
    //console.log(chk_box_id);
  }

  createCheckbox = chkbx => {
    
    var checked = false;
    if (chkbx[0]  == this.props.opencat) {
      //checked = true;
      this.toggleCheckbox(chkbx[1], chkbx[0]);
    }
	
	 const styles: ICheckboxStyles = {
      root: {
        marginLeft: '-40px'
      }
    };
	//return (<div>{chkbx[1]}</div>);
	return (
    <Checkbox
	  name={chkbx[1]}
	  styles={ styles }
      label={chkbx[1]}
      handleCheckboxChange={this.toggleCheckbox}
      key={chkbx[0]}
      chk_box_id={chkbx[0]}
      isChecked = {checked}
      
    />
    );
  }
  
  
  mapThirdLevel = p => ({
	  //name: this.createCheckbox(p),
	  name: p[1],
	  //iconProps: { iconName: 'Delete' },
	  //icon: '',
	  ariaLabel: p[1],
	  title: p[1],
	  onClick: this.toggleCheckbox,
	  key: p[0],
      chk_box_id: p[0]
  })
  
  createPanel = (cid, title, arr) =>{
    return (
    <Panel key={cid} header={<i className="fa fa-sort-desc" aria-hidden="true">{title}..</i> } eventKey={cid}>
    {
           arr.map(this.createCheckbox)      
    }
    </Panel>
    )
  }

  
	fillSubmenu = (t) => {
		var _t = t.map(this.mapThirdLevel);
		return _t;
		//return mapThirdLevel.map(t);
	}
  
  /*drawAcc = (data) => {
    
    for (var cid in data){
                  var rec = data[cid];
                  panelArr.push(this.createPanel(rec[0], rec[1]));
    }
    
    <div>hello!
    </div>
  }*/
    
  render() {
    var chkbx = [];
	var navSecondLevel = [];
	var that = this;

    for (var cid in this.props.data){
      var expand = false;
      //var arr = [cid, this.props.data[cid][0] ];
      var rec = this.props.data[cid];
      chkbx.push(this.createPanel(cid, rec[0], rec[1]));
      if (cid == this.props.opencat) {
        expand = true;
      }
      else {
        for (var arr in rec[1]) {
          if (rec[1][arr][0] == this.props.opencat) {
             expand = true;
          }
        
        }
      }
	  
      navSecondLevel.push({
                  name: rec[0],
                  isExpanded: expand,
                 
                  links: this.fillSubmenu(rec[1]),
                  key: cid
                });
      
	  
    }
	
	
	
    return (
	
		<div className='ms-NavExample-LeftPane'>
        <Nav
          groups={
            [
              {
				isExpanded: true,
                links: navSecondLevel
                
              }
            ]
          }
          expandedStateText={ 'expanded' }
          collapsedStateText={ 'collapsed' }
          selectedKey={ 'key3' }
        />
		
      </div>
	
       
    );
  }
}

/*
 <Accordion >
            {chkbx}
        </Accordion>

,
                  { name: 'Documents', url: 'http://example.com', key: 'key3', isExpanded: true },
                  { name: 'Pages', url: 'http://msn.com', key: 'key4' },
                  { name: 'Notebook', url: 'http://msn.com', key: 'key5' },
                  { name: 'Long Name Test for ellipse', url: 'http://msn.com', key: 'key6' },
                  {
                    name: 'Edit',
                    url: 'http://cnn.com',
                    onClick: this._onClickHandler2,
                    icon: 'Edit',
                    key: 'key8'
                  }
*/
CheckboxGrp2Lvl.propTypes = {
  handleChkgroupChange: PropTypes.func.isRequired,
};
export default CheckboxGrp2Lvl;