import React, { Component } from 'react';
const Footer = () => {
    const style1 = {
        marginTop : "10px"
    };
	
    return (
    <footer className="footer" id="asFooter">
      <div className="container">
      	<div className="row">
      		<div className="col-md-8"><p className="text-muted as-arial as-small" style={style1}>Все права защищены. АГРОСЕЙЛС - поиска товаров для сельхозтоваропроизводителей.
      			<br/><a href="">Политика конфиденциальности</a> | <a href="">Партнерская программа</a> | <a href="">Обратная связь</a></p>

			</div>
      		<div className="col-md-4 text-right">
			
			

			</div>
		</div>
        
      </div>
    </footer>
    )
}
export default Footer;