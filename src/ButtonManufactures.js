import React, { Component, PropTypes } from 'react';

class ButtonManufactures extends Component {
    constructor(props) {
    super(props);
    this.state = {isToggleOn: true,
    ButtonClassName: "Manufactures"
    };

    this.handleClick = this.handleClick.bind(this);
  }
  
  
  handleClick() {
    const { handleCheckboxChange, chk_box_id } = this.props;
    
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
    
    this.state.isToggleOn ? this.setState({ButtonClassName: 'ManufacturesChecked'}) : this.setState({ButtonClassName: 'Manufactures'});
    
    handleCheckboxChange(chk_box_id);
  }
  

 /* toggleCheckboxChange = () => {
    const { handleCheckboxChange, chk_box_id } = this.props;

    this.setState(({ isChecked }) => (
      {
        isChecked: !isChecked,
      }
    ));

    handleCheckboxChange(label, chk_box_id);
  } */

  render() {
    
    const { value } = this.props;
    
    return (
        <label className="ManufacturesLabel" >    
            <button
            type="checkbox"
            value={value}
            className={this.state.ButtonClassName}
            onClick={this.handleClick}>
                <img className="ManufactureImage" src="https://www.basf.com/etc/designs/basf/wcms/base/images/siteicons/logo_twittercard_large.png" alt="BASF"></img>
            </button>
        </label>    
    );
  }
}

ButtonManufactures.propTypes = {
  chk_box_id: PropTypes.number,
  handleCheckboxChange: PropTypes.func.isRequired,
}; 

export default ButtonManufactures;