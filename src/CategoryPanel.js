import React, { Component } from 'react';
import {Panel} from 'react-bootstrap';
import CheckboxGrp2Lvl from './CheckboxGrp2Lvl';


class CategoryPanel extends Component {
 constructor(props) {
    super(props);
    this.state = {
      open: true,
      data:[]
    };

  }
  
  clearCategories() {
   this.refs.categoryGrp.clearChkBoxes();
  }

  render() {
  return (
      <div>
        <label  className="togglemenu" onClick={ ()=> this.setState({ open: !this.state.open })}>
          <i className="fa fa-sort-desc" aria-hidden="true"></i> {this.props.titleBtn}
        </label>
        <Panel className="as-filter as-font" collapsible expanded={this.state.open}>
            <CheckboxGrp2Lvl ref='categoryGrp' data={this.props.data}
            handleChkgroupChange={this.props.chkBoxHandler}
            grpName='categories'
            opencat = {this.props.opencat}/>
        </Panel>
      </div>
    );
  }
}

export default CategoryPanel;