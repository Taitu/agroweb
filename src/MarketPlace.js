import React, {Component} from 'react';
import {Row, Col, Navbar, ButtonToolbar, DropdownButton, MenuItem} from 'react-bootstrap';

import PricesGrid from './PricesGrid';
import SlideChkBoxPanel from './SlideChkBoxPanel';
import CategoryPanel from './CategoryPanel';
import ButtonNavbar from './ButtonNavbar';
import PricesGridHeader from './PricesGridHeader';
import UserMenu from './UserMenu/UserMenu';
import SearchString from './SearchString';
import * as Api from './Api';
import {Fabric} from 'office-ui-fabric-react/lib/Fabric';
import {initializeIcons} from 'office-ui-fabric-react/lib/Icons';
import $ from 'jquery';
import select2 from 'select2';
import 'react-select2-wrapper/css/select2.css';
import Select2 from 'react-select2-wrapper';

initializeIcons(/* optional base url */);

class MarketPlace extends Component {
    constructor(...args) {
        super(...args);
        this.filterBox = [];
        this.state = {
            catopen: false,
            dealers: [],
            addons: [],
            manufacturers: [],
            technologies: [],
            categories: [],
            prices: [],
            searchOptions: [],
            maincats: [],
            filters: [],
            maincat: null,
            last_search: null,
            subcat: null,
            select2Filters: [],
            activeSelect2Item: '',
            //dios: Разные view для поиска
            viewstate: 'default',
        };
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.menuCatHandler = this.menuCatHandler.bind(this);
        this.chkBoxHandler = this.chkBoxHandler.bind(this);
        this.SearchString = "";
    }

    componentWillMount() {
        this.selectedFilters = [];
        this.selectedFilters["page"] = 0;
        this.selectedFilters["limit"] = 10;
        this.cmdSearch = ""


    }

    componentDidMount() {

        if (this.props.match.params.category) {
            if (this.props.match.params.in_search) {
                this.cmdSearch = this.props.match.params.in_search;
                //this.state.last_search = in_search;
                //this.refs.userInput.value = this.props.match.params.in_search;
            }
            if (this.props.match.params.subcat) {
                this.state.subcat = this.props.match.params.subcat;
            }
            Api.call_categories(this.menuCatHandler, this.props.match.params.category);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.location !== this.props.location) {
            if (this.props.match.params.category) {
                if (this.props.match.params.in_search) {
                    this.cmdSearch = this.props.match.params.in_search;
                }
                if (this.props.match.params.subcat) {
                    this.state.subcat = this.props.match.params.subcat;
                }
                Api.call_categories(this.menuCatHandler, this.props.match.params.category);
            }
        }
    }

    setSortParam = (sortId) => {
        this.selectedFilters["sort"] = sortId;
        this.getFilteredPrices();
    };

    // получение отфильтрованного списка данных
    getFilteredPrices() {
        delete this.selectedFilters["product_id"];
        this.selectedFilters["page"] = 0;
        this.selectedFilters["limit"] = 10;
        this.refs.pricesGrid.loadGroups(this.selectedFilters);
    }

    searchBy = (in_search) => {
        if (in_search.length > 0) {
            this.selectedFilters["in_search"] = in_search;
        }
        else {
            delete this.selectedFilters["in_search"]
        }

        this.getFilteredPrices();
        ;

    }


    handleSearchClick = (event) => {
        var that = this;
        this.searchBy(that.refs.userInput.value);

    }

    // callback  для реагирования на ввод данных в строке поиска
    handleKeyPress = (event) => {

        //alert(1); return;

        var that = this;
        // return;
        var in_search = that.refs.userInput.value;
        //alert(ButtonNavbar.rebuildNavbar());
        if (event.which === 13) {
            event.preventDefault();
            //alert(1); return;
            if (in_search.length > 0) {
                this.selectedFilters["in_search"] = in_search;
            }
            else {
                delete this.selectedFilters["in_search"]
            }

            this.getFilteredPrices();
        }
    }

    // callback для реагированиz на checkbox
    chkBoxHandler = (grpName, chkBoxSet) => {
        if (chkBoxSet.size > 0) {
            this.selectedFilters[grpName] = Array.from(chkBoxSet);
        }
        else {
            delete this.selectedFilters[grpName];
        }
        if (grpName === 'categories') {
            var that = this;
            var filters = this.selectedFilters;
            var jsonFilters = Object.assign({}, filters);
            Api.call_catfilters(that.showFilters, jsonFilters);

        }
        this.getFilteredPrices();

    }

    onChange = (e) => {
        if (e.target.value !== '') {
            const jsonFilters = {
                categories: [
                    +e.target.value
                ],
                limit: 10,
                maincat: "1",
                page: 0
            };
            this.selectedFilters.categories = [+e.target.value];

            Api.call_catfilters(this.showFilters, jsonFilters);

            this.getFilteredPrices();

            this.setState({
                activeSelect2Item: e.target.value
            })
        }
    };

    /* Обработчик выбора производителей */
    typeaheadChange = (selectedItems) => {

    }

    manufacturerHandler = (selected) => {
        if (selected.length > 0) {
            this.selectedFilters['manufacturers'] = selected.map(function (item, i, arr) {
                return item['id']
            });
        }
        else {
            delete this.selectedFilters['manufacturers'];
        }
        this.getFilteredPrices();

    }

    showFilters = (dataset) => {
        var currFilter = [this.state.filters[0]];
        var that = this;

        dataset.map(function (item) {
            var dictId = item[0][0];
            var dictName = item[0][1];
            var dictTitle = item[0][2];
            var dictData = item[1];

            currFilter.push(
                <SlideChkBoxPanel ref={(input) => {
                    that.filterBox[dictId] = input
                }} key={dictId} titleBtn={dictTitle}
                                  grpName={dictName} chkBoxHandler={that.chkBoxHandler}
                                  datadict={dictData}
                />
            );
        });
        this.setState({filters: currFilter});
    }

    menuCatHandler = (catId, dataset) => {
        this.selectedFilters = [];
        this.selectedFilters["page"] = 0;
        this.selectedFilters["limit"] = 10;
        this.selectedFilters["maincat"] = catId;
        if (this.cmdSearch) {
            this.selectedFilters["in_search"] = this.cmdSearch
            this.cmdSearch = ""
        }
        if ('cat1' in this.filterBox) {
            this.filterBox['cat1'].clearCategories();
        }
        const select2Filters = [];

        for (let property in dataset) {
            if (dataset.hasOwnProperty(property)) {
                const children = [];
                dataset[property][1].map(p => {
                    children.push({id: p[0], text: p[1]})
                });
                select2Filters.push({text: dataset[property][0], children})
            }
        }

        this.setState({select2Filters});

        setTimeout(() => {
            $(".custom-selection").select2({placeholder: 'Поиск по категории'});

            $('.custom-selection').select2('open')
        }, 500);


        this.getFilteredPrices();
        this.refs.maincatButtons.setButtonActive(catId);
        this.refs.pricesHeader.reloadData(catId);

    }


    // фильтр по конкретному продукту
    setProductFilter = (product_id) => {
        this.selectedFilters["product_id"] = product_id;
    }

// добавляет панель фильтров
    createFilter = (item, flt) => {
        this.state.filters.push(
            <p>{item[0]} </p>
            //<SlideChkBoxPanel titleBtn={item[1]} route={item[0]} chkBoxHandler={this.chkBoxHandler}/>
        )


    }

    onOpen = () => {
        let drawControl = document.getElementsByClassName('select2-search__field');

        drawControl[0].placeholder = 'Введите категорию';
    }

    render() {
        var _i_category = "http://static.agrisale.ru/tools/image_cat.php?action=topcategoryimage&id=" + this.selectedFilters["maincat"];
        var _b_category = "http://static.agrisale.ru/tools/image_cat.php?action=topcategoryimage2&id=" + this.selectedFilters["maincat"];
        var _d_category = "http://static.agrisale.ru/tools/image_cat.php?action=topcategoryimage3&id=" + this.selectedFilters["maincat"];

        return (
            <Fabric>
                <div className="AppAgro">
                    <nav className="navbar navbar-inverse navbar-fixed-top">
                        <div className="container-fluid">
                            <div className="navbar-header">
                                <div className="container-fluid">
                                    <Row>
                                        <Col sm={12} md={2} lg={2}>
                                            <a className="brand-navbar-agri" href="">agri</a>
                                            <a className="brand-navbar-sale" href="">sale</a>
                                            <a className="brand-navbar-bottom" href="">сравнить и купить дешевле</a>
                                        </Col>
                                        <Col sm={10} md={7} lg={7}>
                                            <form>
                                                <SearchString inputRef={(input) => {
                                                    this.SearchString = input
                                                }} id="asSearch" pricesGrid={this.refs.pricesGrid}/>
                                            </form>
                                        </Col>
                                        <Col sm={2} md={3}>
                                            <ButtonToolbar>
                                                <UserMenu/>
                                            </ButtonToolbar>
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                        </div>
                    </nav>

                    <div className="container theme-showcase ontop" role="main">

                        <div className="container-fluid navbar-select-categories">
                            <Row>
                                <Navbar className="select-categories">
                                    <Col xs={0} sm={0} md={12} lg={2}>
                                        EUR / USD / <b>RUB</b>
                                    </Col>
                                    <Col xs={12} sm={12} md={12} lg={10}>
                                        <ButtonNavbar ref="maincatButtons" handleMenuSelect={this.menuCatHandler}/>
                                    </Col>
                                </Navbar>
                            </Row>
                        </div>

                        <div className="container-fluid showcase">
                            <Row className="show-grid">
                                <div id="element"></div>
                                <Col xs={12} md={2} lg={2} className="SlideCheckBoxPanel">
                                    <img src={_i_category} className='maincaticon'/>
                                    <div className='custom-select'>
                                        <div className='custom-select__title'>Категории товаров</div>
                                        <Select2
                                            className='custom-selection'
                                            value={this.state.activeSelect2Item}
                                            onChange={this.onChange}
                                            onOpen={this.onOpen}
                                            data={this.state.select2Filters}
                                        />
                                    </div>
                                    <div className='filters'>
                                        {this.state.filters}
                                    </div>
                                    <img src={_b_category} className='maincaticon'/>
                                    <img src={_d_category} className='maincaticon'/>
                                </Col>

                                <Col xs={12} md={10} lg={10} className="PricesGrid">
                                    <PricesGridHeader ref="pricesHeader"
                                                      setsort={this.setSortParam.bind(this)}
                                                      manufacturerHandler={this.manufacturerHandler}
                                                      maincat={this.selectedFilters["maincat"]}/>
                                    <PricesGrid ref='pricesGrid' selectedFilters={this.selectedFilters}/>
                                </Col>
                            </Row>
                        </div>
                    </div>

                </div>
            </Fabric>
        )
    }
}

export default MarketPlace;