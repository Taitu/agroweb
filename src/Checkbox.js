import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Checkbox as FCheckbox,
  ICheckboxStyles,
  ICheckboxProps
} from 'office-ui-fabric-react/lib/Checkbox';

class Checkbox extends Component {
//class Checkbox extends FCheckbox {

  state = {
    isChecked: false,
  }

  toggleCheckboxChange = () => {
    const { handleCheckboxChange, label, chk_box_id } = this.props;

    this.setState(({ isChecked }) => (
      {
        isChecked: !isChecked,
      }
    ));

    handleCheckboxChange(label, chk_box_id);
  }

  render() {
    const { label, value } = this.props;
    const isChecked  = this.state.isChecked || this.props.isChecked;
 const styles: ICheckboxStyles = {
      root: {
        //marginLeft: '-40px'
      }
    };

    return (
     <div>
		
		<FCheckbox
          label={label}
		  ariaLabel={label}
		  styles={ styles }
		  className="ms-font-s"
		  value={value}
          checked={isChecked}
          onChange={ this.toggleCheckboxChange}
         
        />
		
     </div> 
     
    );
  }
}

Checkbox.propTypes = {
  label: PropTypes.string.isRequired,
  chk_box_id: PropTypes.number,
  handleCheckboxChange: PropTypes.func.isRequired,
};

export default Checkbox;