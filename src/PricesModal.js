import React, { Component } from 'react';
import Modal from 'react-modal';
import CounterInput from 'react-bootstrap-counter';
import * as Api from './Api';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Panel, PanelType } from 'office-ui-fabric-react/lib/Panel';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { MarqueeSelection } from 'office-ui-fabric-react/lib/MarqueeSelection';
import {
  DetailsList,
  DetailsListLayoutMode,
  Selection,
  IColumn
} from 'office-ui-fabric-react/lib/DetailsList';


let _columns_order: IColumn[] = [
  {
    key: 'order_name',
    name: 'Продукт',
	
    fieldName: 'order_name',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: 'Параметр продукта'
  },
   {
    key: 'order_value',
    name: 'Параметр',
	
    fieldName: 'order_value',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: 'Значение параметра'
  },
   {
    key: 'order_price',
    name: 'Цена за ед.',
	
    fieldName: 'order_price',
    minWidth: 100,
    maxWidth: 200,
    isResizable: true,
    ariaLabel: 'Значение параметра'
  },
   {
    key: 'order_count',
    name: 'Количество',
	
    fieldName: 'order_count',
    minWidth: 100,
    maxWidth: 110,
    isResizable: true,
    ariaLabel: 'Значение параметра'
  }]
class PricesModal extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            
        showModal: false,
        price: {},
        orders: [],
        products: {},
        Inn:null,
        Name: null,
        Phone: null,
        Email: null
        
        };
    
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
        
    }
    
    clearBasket = () => {
        this.setState({showModal: false,
                      price: {},
                      orders: [],
                      products: {}
                      });
    }

	
    sendOrder() {
        var order = {
            'name': this.state.Name,
            'phone': this.state.Phone,
            'email': this.state.Email,
            'tin': this.state.Inn,
            'products': this.state.products
            
        }
        Api.send_order(this.clearBasket, order);   
		this.setState({ showModal: false });
				
    }
    
    changeCount = (value, price_id) =>{
        var products = this.state.products;
        if (value === 0) {
            delete products[price_id];
            this.state.orders = []
            this.state.products = products;
            this.filterProduct()
        } else {
            products[price_id]['count'] = value;
            this.setState({products: products});
        }
    }
    
    handleOpenModal (price) {
    var products = this.state.products;
    if (!(price['price_id'] in products)) {
        products[price['price_id']] = price;
        products[price['price_id']]['count'] = 1;
        this.state.products = products;
    }
    this.state.price = price;
    this.state.orders = [];
    this.filterProduct();
    }
  
    handleCloseModal () {
      this.setState({ showModal: false });
    }

    showProduct () {
        if ('product' in this.state.price) {
            return this.state.price['product']['name'];
        }
    }
    
    filterProduct () {
        if (this.state.products != null & Object.keys(this.state.products).length>0) {
            Object.keys(this.state.products).map(this.createOrders.bind(this));
            this.setState({showModal: true})
        } else {
            this.setState({showModal: false})
        }
    }
    
    createOrders = (key, index) => {
        var priceCount = this.state.products[key]['count'];
		this.state.orders.push({
          key: key,
          order_name: <p className="ms-fontColor-themeDark">{ this.state.products[key]['product']['name'] }</p>,
          order_value: this.state.products[key]['price_options'],
		  order_price: this.state.products[key]['price'] + " " + this.state.products[key]['currency'],
		  order_count: <CounterInput value={priceCount} min={1} max={99999}
                                      onChange={ (value) => { this.changeCount(value, key) } }
                        />
        });
		/*
        this.state.orders.push(      
            <table key={key}>
                <tr>
                    <td className="orderImage"></td>
                    <td className="orderMdContent">
                        <p className="productName">{ this.state.products[key]['product']['name'] }</p>
                        <p> {this.state.products[key]['price_options']} </p>
                        <p> {this.state.products[key]['price']} {this.state.products[key]['currency']} </p>
                        <p> {this.state.products[key]['dealer']} </p>
                    </td>
                    <td>
                        <CounterInput value={priceCount}
                                      onChange={ (value) => { this.changeCount(value, key) } }
                        />
                    </td>
                    <td></td>
                </tr>
            </table>
        ); */
    }
  
     _setShowModal(showModal: boolean): () => void {
    return (): void => {
      this.setState({ showModal });
    };
  }
  
  _onRenderFooterContent(): JSX.Element {
    return (
      <div>
        
       
      </div>
    );
  }
    render () {
		var _header = <p><i className="ms-Icon ms-Icon--ShoppingCart" aria-hidden="true"></i> Быстрый заказ</p>;
    return (
			<Panel
			  isOpen={ this.state.showModal }
			  onDismiss={ this._setShowModal(false) }
			  type={ PanelType.medium }
			  headerText= { _header }
			  
			>

			<div className="ms-Grid">
				
				<div className="ms-Grid-row margin-bot-10">
					
					<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg6">
					 <TextField
				   onChanged = {(event) => {this.setState({Name: event})}} 
				  label='Контактное лицо (ФИО)'
				  value={this.state.Name}   
				/>
					</div>
					<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg6">
					<TextField
				   onChanged = {(event) => {this.setState({Phone: event})}} 
				  label='Контактный телефон'
				  value={this.state.Phone}   
				/>
				</div>
				</div>
				<div className="ms-Grid-row margin-bot-10">
					
					<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg6">
					<TextField
					   onChanged = {(event) => {this.setState({Email: event})}} 
					  label='Адрес электронной почты (email)'
					  value={this.state.Email}   
					/>
					
					
					</div>
					<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg6">
					<TextField
				   onChanged = {(event) => {this.setState({Inn: event})}} 
				  label='ИНН организации'
				  value={this.state.Inn}   
				/>
					</div>
				</div>
				
				<p className="ms-font-xl">Список позиций к заказу</p>
				
				<MarqueeSelection selection={ this._selection }>
						  <DetailsList
							items={ this.state.orders }
							columns={ _columns_order }
							setKey='set'
							layoutMode={ DetailsListLayoutMode.justified }
							
							selectionPreservedOnEmptyClick={ true }
							
							onItemInvoked={ this._onItemInvoked }
						  />
						</MarqueeSelection>
			
				<p className="ms-font-xl">Скидки и бонусы</p>
				<div className="ms-Grid-row margin-bot-10 ms-bgColor-greenLight ms-bgColor-green--hover padding-20">
				<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg6"><span className="ms-font-xl">OK!</span> Это самая удачная цена!</div>
				<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg6"></div>
				</div>
				<div className="ms-Grid-row margin-bot-10">
					
					<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg6">
					<PrimaryButton
					  onClick={ this.sendOrder.bind(this) }
					  style={ { 'marginRight': '8px' } }
					>
					  Отправить заказ
					</PrimaryButton>
					</div>
					<div className="ms-Grid-col ms-sm6 ms-md6 ms-lg6"> </div>
				</div>
			</div>
            
                
                
                
                
                
           
			
			</Panel>
        );
    }
}
/*
	<div className="windowModal">

                    {this.state.orders}    
                        
                </div>
*/
//<button onClick={this.handleCloseModal}>Назад</button>
export default PricesModal;
/*{this.state.products.map(this.createOrders())} */