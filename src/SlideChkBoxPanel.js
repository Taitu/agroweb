import React, {Component} from 'react';
import {Panel} from 'react-bootstrap';
import CheckboxGrp from './CheckboxGrp';


class SlideChkBoxPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            data: []
        };

    }

    /*
    componentWillMount() {

        axios.get(process.env.REACT_APP_AGROTRADE_API + this.props.route)
        .then(res => {
                this.setState({data: res.data.dataset.map(LabelValue)});
                //this.setState({searchOptions: arrayColumn(res.data.dealers,1)});
          });
    }

    reloadData(uroute) {

        axios.get(process.env.REACT_APP_AGROTRADE_API + uroute)
        .then(res => {
                this.setState({data: res.data.dataset.map(LabelValue)});
                //this.setState({searchOptions: arrayColumn(res.data.dealers,1)});
          });
    }
    */

    render() {
        const iconCls = !this.state.open ? 'fa-caret-right' : 'fa-caret-down'

        return (
            <div>
                <label className="togglemenu" onClick={() => this.setState({open: !this.state.open})}>
                    <i className={`fa ${iconCls}`} aria-hidden="true"></i>
                    <span>{this.props.titleBtn}</span>
                </label>
                {this.state.open && <Panel className="as-filter as-font" collapsible expanded={this.state.open}>
                    <CheckboxGrp data={this.props.datadict} handleChkgroupChange={this.props.chkBoxHandler}
                                 grpName={this.props.grpName}/>
                </Panel>}
            </div>
        );
    }
}

export default SlideChkBoxPanel;