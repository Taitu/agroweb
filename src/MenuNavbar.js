import React, { Component } from 'react';
import {  Navbar, Nav, NavItem } from 'react-bootstrap';
import axios from 'axios';


class MenuNavbar extends Component {
 constructor(props) {
    super(props);
    this.state = {
      maincats:[]
    };

  }
  // отработка выбора пункта меню
  handleSelect(eventKey) {
    //event.preventDefault();
    var that = this;
    axios.get(process.env.REACT_APP_AGROTRADE_API + '/categories/'+ eventKey)
        .then(res => {
                that.props.handleMenuSelect(eventKey, res.data.dataset);
          });
        
      
  }
  
    componentWillMount() {
        axios.get(process.env.REACT_APP_AGROTRADE_API + '/maincategories')
        .then(res => {
                this.setState({maincats: res.data.dataset});
          });
    }
    
    createMenuItem = item => (
      <NavItem key={item[0]} eventKey={item[0]} href="#"  >{item[1]}</NavItem>
    )


    
    render() {    
    
    return (
        <Navbar>
          <Nav  onSelect={this.handleSelect.bind(this)}>
                {this.state.maincats.map(this.createMenuItem)}
          </Nav>
        </Navbar>
    );
    
    }
}
export default MenuNavbar;
