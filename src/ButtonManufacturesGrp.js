import React, { Component, PropTypes } from 'react';
import ButtonManufactures from './ButtonManufactures';



class ButtonManufacturesGrp extends Component {

  
  componentWillMount = () => {
    this.selectedCheckboxes = new Set();
  
  }

  toggleCheckbox = (chk_box_id) => {

    if (this.selectedCheckboxes.has(chk_box_id)) {
      this.selectedCheckboxes.delete(chk_box_id);
    } else {
      this.selectedCheckboxes.add(chk_box_id);
    }
   /*this.props.handleChkgroupChange(this.props.grpName,this.selectedCheckboxes);
    console.log(chk_box_id)*/
  }

  createButtonManufactures = chkbx => (
    <ButtonManufactures
      handleCheckboxChange={this.toggleCheckbox}
      key={chkbx.value}
      chk_box_id={chkbx.value}
     />
  )

  render() {
    return (
        <div>
            {
                this.props.data.map(this.createButtonManufactures)
            }
        </div>
    );
  }
}

ButtonManufacturesGrp.propTypes = {
  handleChkgroupChange: PropTypes.func.isRequired,
};
export default ButtonManufacturesGrp;