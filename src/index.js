import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import { Provider } from 'react-redux';
import dotenv from 'dotenv';
import MarketPlace from './MarketPlace';
import { store } from './Store/Store';
import './index.css';
import './App.css';

import { signIn, setTokenWithSocial } from 'UserMenu/actions';

dotenv.config({ silent: true });

render(
  <Provider store={store}>
      <BrowserRouter>
          <Switch>
              <Route exact path="/" component={() => <Redirect to="/search/category/1"/>}/>
              <Route exact path="/search" component={MarketPlace}/>
			 /* <Route exact path="/search" render={props => {
					return <Redirect to="/search/category/1"/>
			  }}/> */
              <Route path="/search/activate/:token" render={props => {
                store.dispatch(signIn(props.match.params.token, true));
                return <MarketPlace {...props}/>
              }}/>
              <Route path="/reset_password/:token" render={props => {
                store.dispatch(signIn(props.match.params.token, true));
                return <MarketPlace {...props}/>
              }}/>
              <Route path="/search/category/:category/search/:in_search" component={MarketPlace}/>
              <Route path="/search/category/:category/subcat/:subcat" component={MarketPlace}/>
              <Route path="/search/category/:category" component={MarketPlace}/>
              <Route path="/search/product/:product" component={MarketPlace}/>
              <Route path="/oauth/:socialName/callback" render={props => {
                store.dispatch(setTokenWithSocial(`/oauth/${props.match.params.socialName}/token${props.location.search}`));
                return <Redirect to="/search"/>
              }}/>
          </Switch>
      </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
