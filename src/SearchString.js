import React, {Component} from 'react';
import {AsyncTypeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import './SearchStringStyle.css';
import axios from 'axios';
import {withRouter, Link} from "react-router-dom";

class SearchString extends Component {
    state = {
        isLoading: false,
        options: [],
        response: undefined,
        queryString: ''
    };

    render() {
        return (
            <div className="input-group">
                <div>
                    <AsyncTypeahead
                        {...this.state}
                        ref={(typeahead) => this.typeahead = typeahead}
                        labelKey="name"
                        minLength={2}
                        onSearch={this._handleSearch}
                        emptyLabel="По вашему запросу ничего не найдено"
                        searchText="Ищу..."
                        placeholder="Что вы хотите найти..."
                        promptText="Введите запрос"
                        filterBy={option => option}
                        onChange={(selected) => {
                            if (selected && selected.length > 0 && "link" in selected[0]) {
                                var lnk = selected[0].link;
                                this.props.history.replace(lnk)
                            }
                        }}
                        onKeyDown={this.enterPressed}
                        renderMenuItemChildren={(option) => (
                            <div>
                                <p>{option.name}
                                    <i> в {option.category + "/" + option.subcategory + "/" + option.sort}</i>
                                    <br/>
                                    <span>Производитель: {option.manufacture}</span>
                                </p>
                            </div>
                        )}
                    />
                </div>
                <span className="input-group-btn" onClick={this.loadData}>
                    <label className="btn btn-success as-no-round" type="label">
                        <i className="fa fa-search" aria-hidden="true"></i> Найти дешевле
                    </label>
                </span>
            </div>
        );
    }

    enterPressed = (e) => {
        if (e.keyCode === 13) {
            this.loadData()
        }
    };

    loadData = () => {
        const {response, queryString} = this.state;
        if (response !== undefined) {
            this.typeahead.getInstance().blur();
            const option = response[0];
            if (response.length === 1) {
                this.props.history.replace('/search/category/' + option[6] + '/search/' + queryString)
                this.props.pricesGrid.loadSingleViaSearch(option[5])
            } else {
                this.props.history.replace('/search/category/' + option[6] + '/search/' + queryString)
            }
        }
    }

    _handleSearch = (query) => {
        const that = this;
        that.setState({isLoading: true});
        axios.get(process.env.REACT_APP_AGROTRADE_API + '/suggestions/' + query)
            .then(function (response) {
                that.setState({
                    isLoading: false,
                    options: response.data.map((option) => ({
                        manufacture: option[4],
                        name: option[3],
                        sort: option[2],
                        subcategory: option[1],
                        category: option[0],
                        link: '/search/category/' + option[6] + '/search/' + option[3]
                    })),
                    response: response.data,
                    queryString: query
                });
            });
    }
}

export default withRouter(SearchString);
